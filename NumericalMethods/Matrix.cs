﻿namespace NumericalMethods;

public class Matrix {
    public float[,] MatrixArray { get; set; }
    public int Length => MatrixArray.GetLength(0);

    public Matrix(float[,] matrix) { MatrixArray = matrix; }

    public float this[int i, int j] { get => MatrixArray[i, j]; set => MatrixArray[i, j] = value; }

    public static float Determinant(float[,] matrix) {
        switch (matrix.GetLength(0)) {
            case 1:
                return matrix[0, 0];
            case 2:
                return matrix[0, 0] * matrix[1, 1] - matrix[1, 0] * matrix[0, 1];
        }

        int sign = 1;
        var sum = 0.0f;
        for (int i = 0; i < matrix.GetLength(0); i++) {
            sign = i % 2 == 0 ? 1 : -1;
            sum += matrix[0, i] * sign * Determinant(ExcludeElement(matrix, i));
        }

        return sum;
    }

    public float[,] ReplaceColumn(int col, float[] value) {
        if (col >= Length) {
            col = Length;
            var resultMatrix = new float[col + 1, col + 1];
            for (int i = 0; i < col + 1; i++) {
                for (int j = 0; j < col; j++) {
                    if (i == col) { resultMatrix[i, j] = value[j]; } else { resultMatrix[i, j] = MatrixArray[i, j]; }
                }
            }

            return resultMatrix;
        }

        var newMatrix = (float[,])MatrixArray.Clone();
        for (int i = 0; i < newMatrix.GetLength(0); i++) { newMatrix[i, col] = value[i]; }

        return newMatrix;
    }

    private static float[,] ExcludeElement(float[,] matrix, int col) {
        var newMatrix = new float[matrix.GetLength(0) - 1, matrix.GetLength(0) - 1];
        int newI = 0, newJ = 0;
        for (int i = 0; i < matrix.GetLength(0); i++) {
            if (newJ >= newMatrix.GetLength(0) - 1) {
                newI++;
                newJ = 0;
            }

            for (int j = 0; j < matrix.GetLength(0); j++) {
                if (i != 0 && j != col) { newMatrix[newI, newJ++] = matrix[i, j]; }
            }
        }

        return newMatrix;
    }

    public static float[,] JordanExcluding(float[,] matrix) {
        var newMatrix = (float[,])matrix.Clone();
        for (int i = 0; i < newMatrix.GetLength(0); i++) {
            for (int j = i; j < newMatrix.GetLength(0); j++) {
                for (int k = i + 1; k < newMatrix.GetLength(0); k++) {
                    if (j == i) {
                        newMatrix[i, j] *= -1;
                        continue;
                    }

                    newMatrix[i, j] = newMatrix[i, i] * matrix[j, k] - newMatrix[i, k] * newMatrix[j, i];
                }
            }

            newMatrix[i, i] = 1 / newMatrix[i, i];
        }

        return newMatrix;
    }
}