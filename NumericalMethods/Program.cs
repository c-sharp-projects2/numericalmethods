﻿using NumericalMethods;
using NumericalMethods.Exceptions;

public class Program {
    static void Main() {
        var matrix = new Matrix(new float[,] { { 2, -1, 0, 1 }, { 2, 3, 1, -3 }, { 3, 4, -1, 2 }, { 1, 3, 1, -1 } });
        var freeColumn = new float[] { -3, -6, 8, -5 };

        // try { Kramer(matrix, freeColumn); } catch (NotSolvesException e) { Console.WriteLine(e); }
        JordanGauss(matrix, freeColumn);
    }

    private static float[] Kramer(Matrix matrix, float[] freeColumn) {
        var determinant = Matrix.Determinant(matrix.MatrixArray);
        if (determinant == 0) { throw new NotSolvesException("Equation has not solves"); }

        var solves = new float[matrix.Length];
        for (int i = 0; i < matrix.Length; i++) {
            solves[i] = Matrix.Determinant(matrix.ReplaceColumn(i, freeColumn)) / determinant;
        }

        return solves;
    }

    private static float[] JordanGauss(Matrix matrix, float[] freeColumn) {
        for (int i = 0; i < freeColumn.Length; i++) {
            freeColumn[i] *= -1;
        }

        var resultMatrix = Matrix.JordanExcluding(matrix.ReplaceColumn(matrix.Length, freeColumn));
        var solves = new float[resultMatrix.GetLength(0) - 1];
        for (int i = 0; i < resultMatrix.GetLength(0) - 1; i++) {
            var sum = 0.0f;
            for (int j = 0; j < resultMatrix.GetLength(0) - 1; j++) {
                if (j == i) { continue; }

                if (j < i) {
                    sum += solves[j] * resultMatrix[i, j];
                    continue;
                }

                break;
            }

            sum -= -resultMatrix[i, resultMatrix.GetLength(0) - 1];
            solves[i] = sum / resultMatrix[i, i];
        }

        return Array.Empty<float>();
    }
}