﻿namespace NumericalMethods.Exceptions;

public class NotSolvesException : Exception {
    public NotSolvesException(string message) : base(message) { }
}